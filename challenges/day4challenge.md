## Day 4 Skills Challenge!

<img src="https://miro.medium.com/max/669/1*sssWakAf5erMGDqt9GACVA.jpeg" width="500"/>

Below is an existing playbook- it's not particularly fancy! Please make the following changes:

- Configure the play so that you can choose to only run `first task`, `second task`, and `fourth task` when executing the playbook. [Need a hint? Click here!](https://docs.ansible.com/ansible/latest/user_guide/playbooks_tags.html#adding-tags-to-individual-tasks)

- Configure the `FAIL TASK` so that it NEVER runs unless explicitly told that it should. [Need a hint? Click here!](https://docs.ansible.com/ansible/latest/user_guide/playbooks_tags.html#special-tags-always-and-never)

- Add a `handlers` section. Create a task in the handlers section named `my handler task`. What the `my handler task` does is up to you! Then configure the `third task` so that it notifies `my handler task` to run. **Need a hint? Look at Lab 29, step 19!**

- Put ALL the tasks (not the handler) in this play inside a block. Then, add a `rescue` to that block! What the rescue does is up to you! **Need a hint? Look at Lab 30, step 11!!**

```yaml
---
- name: Day 4 Challenge
  hosts: localhost
  connection: local
  gather_facts: no

  vars:
      four_task: This is the fourth task!

  tasks:

  - name: first task
    debug:
        msg: “This is the first task!”

  - name: second task
    debug:
        msg: “This is the second task!”

  - name: third task
    shell: echo “This is the third task!”

  - name: fourth task
    debug:
        var: four_task

  - name: FAIL TASK
    fail:
       msg: “This task will intentionally fail.”
```

### SOLUTION

```yaml
---
- name: Day 4 Challenge
  hosts: localhost
  connection: local
  gather_facts: no

  vars:
      four_task: This is the fourth task!

  tasks:

  - name: this is a block
    block:

          - name: first task
            debug:
                msg: â€œThis is the first task!â€
            tags:
            - demo

          - name: second task
            debug:
                msg: â€œThis is the second task!â€
            notify:
            - my handler task
            tags:
            - demo

          - name: third task
            shell: echo â€œThis is the third task!â€

          - name: fourth task
            debug:
                var: four_task
            tags:
            - demo

          - name: FAIL TASK
            fail:
               msg: â€œThis task will intentionally fail.â€
            tags:
            - never

    rescue:

    - name: rescue task
      debug:
        msg: "This is a rescue task!"

  handlers:
  - name: my handler task
    debug:
        msg: â€œThis is the handler task!â€
```
