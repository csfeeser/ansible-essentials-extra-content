# Day 3 Morning Challenge

<img src="https://www.meme-arsenal.com/memes/83729f36212c1241083847dcc59beaec.jpg" width="500"/>

Today we'll attempt to bring together some of the skills we've learned in the last two days in a custom playbook written by you!

Run the following command to configure your environment for this challenge:

`student@bchd:~$` `cd && wget https://labs.alta3.com/projects/ansible/deploy/day3challenge.sh && bash day3challenge.sh`

The planetexpress hosts are up and running... but **WARNING**: `bender`, `fry`, and `zoidberg` do not have Python installed!

Write a playbook that does the following:

- Runs against `bender`, `fry`, and `zoidberg` but NOT `farnsworth`
- Installs Python on the hosts
- Create a var for this url: https://httpstatusdogs.com/img/200.jpg
- Download the image from that url to all hosts in their `/tmp` directory.

**BONUS:**

Write a LOOP to download multiple pictures! Use the following urls to list them out:

https://httpstatusdogs.com/img/404.jpg  
https://httpstatusdogs.com/img/420.jpg  
https://httpstatusdogs.com/img/418.jpg  

**SUPER BONUS:**

When saving the picture file to the hosts, include the host's name in the new picture's file name!

> Example: `bender-200.jpg  bender-404.jpg  bender-418.jpg  bender-420.jpg`

HINT: check out the **basename** filter! [Click here to see how it is used!](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#managing-file-names-and-path-names)


```yaml
---
- name: bootstrapping and new user
  hosts: planetexpress:!farnsworth
  connection: ssh  
  gather_facts: False

  vars:
    urls:
      - https://httpstatusdogs.com/img/200.jpg
      - https://httpstatusdogs.com/img/404.jpg
      - https://httpstatusdogs.com/img/420.jpg
      - https://httpstatusdogs.com/img/418.jpg

  tasks:

  - name: "Bootstrap python install"
    raw: "apt install -y python3-pip"
    become: yes

  - name: download cute dogs
    get_url:
      url: "{{ item }}"
      dest: "/tmp/{{ ansible_user + '-' + item|basename }}"
    loop: "{{ urls }}"
    tags:
      - wip
```
